package edu.usna.mobileos.recyclerviewshell

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.RecyclerView

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Sample data
        val rssTitles = listOf("Title 1", "Title 2", "Title 3", "Title 4")

        // Set up the RecyclerView
        val rssAdapter = RssItemAdapter(rssTitles)
        findViewById<RecyclerView>(R.id.rss_recycler).adapter = rssAdapter
    }
}
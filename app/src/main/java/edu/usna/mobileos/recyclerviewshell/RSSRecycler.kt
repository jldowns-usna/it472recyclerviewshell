package edu.usna.mobileos.recyclerviewshell

import android.view.ContextMenu
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView


class RssItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    val rssTitle: TextView = view.findViewById(R.id.title)

}


class RssItemAdapter(var data: List<String>) :
    RecyclerView.Adapter<RssItemViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RssItemViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_layout, parent, false)
        return RssItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: RssItemViewHolder, position: Int) {
        val rssTitle = data[position]
        holder.rssTitle.text = rssTitle
    }

    override fun getItemCount(): Int {
        return data.size
    }
}